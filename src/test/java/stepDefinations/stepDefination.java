package stepDefinations;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class stepDefination {

	WebDriver driver;
	
    @Given("^I am on google search page$")
    public void i_am_on_google_search_page() throws Throwable 
    {
    	
    	System.setProperty("webdriver.chrome.driver","C:\\Users\\Gustavo\\Documents\\TechMahindra\\Selenium\\chromedriver_win32\\chromedriver.exe");
		
		driver = new ChromeDriver();
		
		driver.get("https://www.google.com");

		driver.manage().window().maximize();
    }
    
    @When("^Enter \"([^\"]*)\" keyword$")
    public void enter_something_keyword(String Selenium) throws Throwable 
    {
       driver.findElement(By.name("q")).sendKeys(Selenium);
    }
    
    @Then("^Click on Search button$")
    public void click_on_search_button() throws Throwable 
    {
       driver.findElement(By.name("btnK")).submit();
    }
    
    @And("^Get results related to the keyword entered$")
    public void get_results_related_to_the_keyword_entered() throws Throwable 
    {
      Thread.sleep(3000);
      Assert.assertEquals(driver.getTitle(), "Selenium - Buscar con Google");
    }
    
    @And("^Get results related to the keyword entered on text field$")
    public void get_results_related_to_the_keyword_entered_on_text_field() throws Throwable {
        Thread.sleep(3000);
        Assert.assertEquals(driver.getTitle(), "Bogotá - Buscar con Google");
    }
	
}

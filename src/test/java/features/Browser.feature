Feature: Searching key words on Chrome Browser
	I want to search two keys words on Google Chrome browser
	
	Scenario: As final customer, I need searching the word Selenium on chrome Browser. 
	Given I am on google search page
	When Enter "Selenium" keyword
	Then Click on Search button
	And Get results related to the keyword entered
	
	Scenario: As Customer Service agent, I need searching the word Bogotá on chrome Browser.  
	Given I am on google search page
	When Enter "Bogotá" keyword
	Then Click on Search button
	And Get results related to the keyword entered on text field
	
	